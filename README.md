# Custom SD image for Raspberry Pi 4

This repository contains the instructions and configuration to build a SD image with a bigger /boot partition size and my SSH public keys

## Building

1. Enable QEMU on the host machine by adding the following option

```{nix}
boot.binfmt.emulatedSystems = [ "aarch64-linux" ];
```

2. Build the image

```{bash}
nix-build '<nixpkgs/nixos>' -A config.system.build.sdImage -I nixos-config=./sd-image.nix --argstr system aarch64-linux
```

## Usage

1. Extract the image

```{bash}
nix-shell -p zstd --run "unzstd rp4-image.img.zst"
```

2. Burn it to an SD card

``` bash
sudo dd if=rp4-image.img of=/dev/mmcblk0 bs=1M status=progress conv=fsync
```
